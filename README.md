# Tiny Weather Forecast Germany Group

[![Framagit ci/cd -> pipeline status](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanygroup.frama.io/badges/main/pipeline.svg)](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanygroup.frama.io/-/commits/main) [![F-Droid Store release version](https://img.shields.io/f-droid/v/de.kaffeemitkoffein.tinyweatherforecastgermany?color=%23efbb24&logo=fdroid&style=for-the-badge)](https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany) [![code license](https://img.shields.io/github/license/twfgcicdbot/TinyWeatherForecastGermanyMirror?style=for-the-badge&logo=gitlab)](https://gitlab.com/tinyweatherforecastgermanygroup/twfg-repo-mirror-cicd/-/blob/00142082d3e4fc71cc27daf07c5f42c6f9a4ad2e/LICENSE)

[-> see here for the GitLab Pages page of **this** repository (hosted by Framagit)](https://tinyweatherforecastgermanygroup.frama.io)

[-> see here for the overview GitLab Pages page of this project (hosted by GitLab)](https://tinyweatherforecastgermanygroup.gitlab.io/index)

[-> see here for the code **docs**](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/overview-summary.html)

**TL;DR** source of the index/main GitLab Pages web site at Framagit of TinyWeatherForecastGermanyGroup

## Purpose -> *Why?*

**Target:** quick overview of all content and workflows related to the Project [**TinyWeatherForecastGermany**](https://codeberg.org/Starfish/TinyWeatherForecastGermany) by Pawel Dube ([@Starfish](https://codeberg.org/Starfish)) at Codeberg, Gitlab, GitHub, Gitea and Framagit.

***Note**: 'TWFG' is an inofficial abbreviation for **T**iny **W**eather **F**orecast **G**ermany.*

## Reason -> *What's the added value?*

* increased visibility (e.g. by better [SEO](https://en.wikipedia.org/wiki/Search_engine_optimization) scores) resulting in a broader audience while directing contributors to the *main* repository at *codeberg.org* to keep it as the central location for all activities of the user and dev community

## Workflow -> *How does it work?*

**tl;dr** -> it runs completely **autonomously** once per week on Tuesday at 5am UTC on shared runners of **Framagit CI/CD**

please see the CI/CD pipeline defined in [`.gitlab-ci.yml`](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanygroup.frama.io/-/blob/main/.gitlab-ci.yml) for details.

## License/Copyright and Credits

GNU **G**ENERAL **P**UBLIC **L**ICENSE (**GPLv3**) please see the [`License`](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanygroup.frama.io/-/blob/962a42b96c0fade3c27ec8e5a0d7da65a1cf26c7/LICENSE) file for details. Please also see the legal terms noted inline in the code base.

**Copyright** of the *main* project -> **Tiny Weather Forecast Germany** -> Pawel Dube ([@Starfish](https://codeberg.org/Starfish))

This CI/CD script was created by Jean-Luc Tibaux (->@eUgEntOptIc44)

The **GZIP and brotli compression** feature (high priority for SEO) was integrated using the following [documentation](https://docs.gitlab.com/ee/user/project/pages/introduction.html#serving-compressed-assets) chapter.

Please be aware that the code used here is by no means production-ready. Before using even parts of this in professional/commercial environments please double check any local laws and regulations that might apply in your region or country. In general your mileage might **significantly** vary. Use only at your **own risk**. No guarantee of any kind provided. The author highly encourages everyone reading this to get in touch before using any of the contents of this repository for commercial or by any means critical applications.

This project is not affiliated with any of the following organizations: Codeberg, the DWD, GitLab, GitHub, Gitea or related individuals in any way. Named trademarks and brands belong to their owners.

## Contributing

* As noted above contributions to **Tiny Weather Forecast Germany** are managed at the [*'main'*](https://codeberg.org/Starfish/TinyWeatherForecastGermany) code repository at [Codeberg](https://codeberg.org/Starfish/TinyWeatherForecastGermany)
* [**Translations**](https://translate.codeberg.org/engage/tiny-weather-forecast-germany/) are managed at the [**Weblate** instance](https://translate.codeberg.org/projects/tiny-weather-forecast-germany/) provided by Codeberg e.V.
* Feel free to contribute to this script by opening issues and/or merge requests.
* Please also see the automatically generated **javadoc code documentation** of Tiny Weather Forecast Germany [here](https://gitlab.com/tinyweatherforecastgermanygroup/twfg-javadoc).
* For CyberSec, privacy and/or copyright related issues regarding this repository please directly contact the maintainer **Jean-Luc Tibaux** (@eUgEntOptIc44)
