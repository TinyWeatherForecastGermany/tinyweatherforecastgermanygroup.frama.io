site_name: TinyWeatherForecastGermany
site_description: An open source android weather app focused on Germany
site_url: https://tinyweatherforecastgermanygroup.frama.io/
repo_url: https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanygroup.frama.io
edit_uri: ""

nav:
    - Home: index.md
    - Help: help.md
    - Licenses: licenses.md

theme:
  name: material
  font: false
  features:
   # - navigation.instant # deactivated to fix https://github.com/ultrabug/mkdocs-static-i18n/issues/62
   - navigation.tabs
   - navigation.tracking
   - navigation.sections
   - navigation.indexes
   - navigation.top
   - toc.integrate
  palette:
   - media: "(prefers-color-scheme: light)"
     scheme: default
     accent: teal
     primary: blue
     toggle:
        icon: material/toggle-switch-off-outline
        name: Switch to dark mode
   - media: "(prefers-color-scheme: dark)"
     scheme: slate
     primary: blue
     accent: teal
     toggle:
        icon: material/toggle-switch
        name: Switch to light mode
  logo: images/icon.png
  favicon: images/favicon.ico
  custom_dir: overrides
  icon:
     repo: fontawesome/brands/gitlab

plugins:
   - search:
      lang:
        - en
        - de
        - fr
   - minify:
      minify_html: true
      minify_js: true
      htmlmin_opts:
        remove_comments: true
      css_files:
         - stylesheets/extra.css
   - i18n:
      default_language: en
      languages:
        - locale: en
          name: English
          default: true
          build: true
        - locale: fr  
          name: Français
        - locale: de
          name: Deutsch
   - git-revision-date-localized:
      type: datetime
      enable_creation_date: true

markdown_extensions:
   - toc:
      toc_depth: 3
      permalink: true
      slugify: !!python/object/apply:pymdownx.slugs.slugify {kwds: {case: lower, percent_encode: true}}
   - attr_list
   - footnotes
   - pymdownx.highlight
   - pymdownx.superfences
   - pymdownx.tabbed
   - pymdownx.critic
   - pymdownx.betterem:
      smart_enable: all
   - pymdownx.caret
   - pymdownx.mark
   - pymdownx.tilde
   - pymdownx.smartsymbols 
   - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
      options:
         custom_icons:
            - overrides/.icons
         image_path: images/twemoji/svg/
   - pymdownx.details
   - pymdownx.arithmatex: # preserve LaTex code -> requires including MathJax -> https://facelessuser.github.io/pymdown-extensions/extensions/arithmatex/
      generic: true
   - pymdownx.tasklist: # needs extra css -> https://facelessuser.github.io/pymdown-extensions/extensions/tasklist/
      custom_checkbox: true
   - pymdownx.magiclink # recognize links in plaintext
   - pymdownx.keys # to display keyboard keys
   - pymdownx.progressbar # needs extra css -> https://facelessuser.github.io/pymdown-extensions/extensions/progressbar/
   - pymdownx.escapeall # escape ALL non standard chars -> https://facelessuser.github.io/pymdown-extensions/extensions/escapeall/
   - abbr
   - admonition
   - meta
   - def_list

extra:
  manifest: images/site.webmanifest

extra_css:
  - stylesheets/extra.css

copyright: Copyright &copy; 2021-2024 Jean-Luc Tibaux (@eugenoptic44)
